<?php

namespace WsClient\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use WsClient\Model\UtilisateurQuery;
use Zend\Session\Container;

/**
 * Gère la connexion au service web
 */
class ConnectionController extends AbstractRestfulController {

    private function cryptToMySqlPassword($password) {
        $pass = strtoupper(
                sha1(
                        sha1($password, true)
                )
        );
        $pass = '*' . $pass;
        return $pass;
    }

    /**
     * Vérifie l'existance du login et mot de passe dans la table utilisateur
     * 
     * @param Array les informations transmises en paramètre
     * @return JsonModel Un mesasge encodé en JSON
     * connected:{true=1/false=0}
     */
    public function create($data) {

        $resultat = false;
        $login = $data['login'];
        $password = $data['password'];
        $cryptPassword = $this->cryptToMySqlPassword($password);
        $utilisateur = UtilisateurQuery::create()->findOneByArray(
                array(
                    'login' => $login,
                    'password' => $cryptPassword,
                    'type' => 'client',
                )
        );
        if ($utilisateur != null) {
            $container = new Container('utilisateur');
            $container->client = $utilisateur;
            $resultat = true;
        }
        return new JsonModel(array(
            'connected' => $resultat
        ));
    }

}
