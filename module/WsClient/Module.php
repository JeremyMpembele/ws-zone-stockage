<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace WsClient;


use Zend\Mvc\MvcEvent;
use Zend\Session\Container;
use Zend\Http\Response;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $eventManager->attach(MvcEvent::EVENT_DISPATCH, function($e){
            $session = new Container('Utilisateur');
            $controllerName = $e->getRouteMatch()->getParam('controller');
            if ($controllerName != 'Connection' && $session->client == null)
            {
                $response = new Response();
                $response->setStatusCode(Response::STATUS_CODE_200);
                $response->setContent('Connexion nécessaire');
                $e->stopPropagation(true);
                return $response;
            }
        }, 100);
    }

    public function getConfig()
    {
        require_once __DIR__ . '/config/propel.config.php';
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
}
