<?php


return array(
    'router' => array(
        'routes' => array(
            'rWsClient' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/wsClient',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'WsClient\Controller',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'rWsClientChild' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/[:controller]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z0-9_-]+',
                            ),
                            'defaults' => array(
                                '__NAMESPACE__' => 'WsClient\Controller'
                            ),
                        ),
                    ),
                ),
            ),
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Connection' => 'WsClient\Controller\ConnectionController',
            'Reservation' => 'WsClient\Controller\ReservationController',
        ),
    ),
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
);
